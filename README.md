# Add an Autowire trait for plugins

This module adds an autowire trait for plugins.

This issue suggests adding it to Drupal core:
[#3452852](https://www.drupal.org/project/drupal/issues/3452852).

This module implements the suggested trait as a contrib module for use
now. The implementation is borrowed shamelessly and with thanks to the
core issue.
